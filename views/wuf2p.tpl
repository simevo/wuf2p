{{ !get('issue') }}

% if defined('screenshot_id'):
attached screenshot: {F{{ get('screenshot_id') }}}

% end
server info:
===========
| hostname | {{ get('fqdn', '') }} |
| machine-id | `{{ get('machine_id', '') }}` |

client info:
===========

| local time | {{ get('timeOpened', '') }} |
| timezone | {{ get('timezone', '') }} |
| navigator.appName | {{ get('browserName', '') }} |
| navigator.product | {{ get('browserEngine', '') }} |
| navigator.appVersion | `{{ get('browserVersion1a', '') }}` |
| navigator.userAgent | `{{ get('browserVersion1b', '') }}` |
| navigator.language | {{ get('browserLanguage', '') }} |
| navigator.onLine | {{ get('browserOnline', '') }} |
| navigator.platform | {{ get('browserPlatform', '') }} |
| navigator.javaEnabled() | {{ get('javaEnabled', '') }} |
| navigator.cookieEnabled | {{ get('dataCookiesEnabled', '') }} |
| screen.width x screen.height | {{ get('sizeScreenW', '') }} x {{ get('sizeScreenH', '') }} |
| document.width x document.height | {{ get('sizeDocW', '') }} x {{ get('sizeDocH', '') }} |
| innerWidth x xinnerHeight | {{ get('sizeInW', '') }} x {{ get('sizeInH', '') }} |
| screen.availWidth x screen.availHeight | {{ get('sizeAvailW', '') }} x {{ get('sizeAvailH', '') }} |
| screen.colorDepth | {{ get('scrColorDepth', '') }} |
| screen.pixelDepth | {{ get('scrPixelDepth', '') }} |

webapp info:
===========
| url | {{ get('url', '') }} |
| window.location.pathname | {{ get('pageon', '') }} |
| document.referrer | {{ get('referrer', '') }} |
| document.cookie | `{{ get('dataCookies1', '') }}` |
| localStorage | {{ get('dataStorage', '') }} |
