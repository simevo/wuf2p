**wuf2p** (webapp user feedback to phabricator) is a web application plugin for collecting user feedback and sending it straight to Phabricator, creating a task in Maniphest complete with screenshot and detailed info.

It is made of two parts:

- a HTML5/CSS/js frontend component

- a python backend written with [bottle](http://bottlepy.org/).

# License

MIT License, (C) Copyright 2016 Paolo Greppi simevo.com

Credits:

- [Phacility](https://www.phacility.com/) for the excellent Phabricator software development platform

- [Bootstrap Jumbotron example](http://getbootstrap.com/examples/jumbotron/): index.html + css/jumbotron.css

- Stephan Wagner's [CSS-only loading spinner](http://stephanwagner.me/only-css-loading-spinner): css/wuf2p.css

- Michael Spector's [bootstrap-feedback-form](https://github.com/spektom/bootstrap-feedback-form) as a basis for the user feedback UI

# HOW it works

1. User visits the web application with their browser, and sees a sticky Feedback button:

    ![demo](img/ss1.png)

2. User clicks on the Feedback button and fills in the form:

    ![demo_filling](img/ss2.png)

3. On clicking Send, the browser creates a screenshot using [html2canvas](https://github.com/niklasvh/html2canvas), collects a number of information, then submits all that with a POST request to the backend `/feedback`. While that happens, a spinner is shown:

    ![demo_spinner](img/ss3.png)

4. The backend responds on that endpoind, and forwards the informations to the Phabricator instance, using the [Conduit API](https://secure.phabricator.com/book/phabricator/article/conduit/) to create a task in Maniphest. On success, an alert is shown:

    ![demo_success](img/ss4.png)

5. The developers can now see the task in Phabricator, something like:

    ![demo_phabricator](img/ss6.png)

    the task also contains as attachment the screenshot from the client browser:

    ![demo_client_screenshot](img/ss5.png)

# Try it out

1. get [bower](https://bower.io/)

2. install the required python stuff: `apt-get install python python-requests python-bottle`

3. do `bower install` to install the frontend dependencies

4. move the files from `bower_components` to `css` / `js` / `fonts` with the command `./copy_files.sh`

5. edit the `phabricator.py` file and modify the `phabricator_instance` (should be the FQDN of the Phabricator instance where you want to file the task), the `api_token` (can be obtained as follows: as a Phabricator admin, create a [bot account](https://secure.phabricator.com/book/phabricator/article/users/#bot-accounts) then "Edit Settings", go to "Conduit API Tokens", click "Generate API token") and the `projectPHID` (the Phabricator ID of the project you want to file your task against; [here](http://stackoverflow.com/a/25754181) is how you can find that out)).
 
6. launch the backend: `./wuf2p.py`

7. visit the test page: http://localhost:8080/

# TODOs

- the Phabricator developers are [planning](https://secure.phabricator.com/T8783) a new application called Nuance which will be "a high-volume human-intelligence task queue". When an API becomes available for that, it would be better for the user feedback to be sent to that queue rather than create a task straightaway so that the feedbacks can be triaged

- there are [better ways](http://stackoverflow.com/a/28090234) to capture the screenshot

- replace the `copy_files.sh` script with a JS build tool 

- after submitting the user feedback, it would be nicer to display the success / failure with a bootstrap modal rather than an alert
