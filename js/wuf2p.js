// configuration for jshint
/* jshint browser: true, devel: true */
/* global html2canvas, $
 */
"use strict";

$(function() {
  $("#user-feedback-tab").click(function() {
    $("#user-feedback-form").toggle("slide");
  });
  $("#user-feedback-cancel").click(function() {
    $("#user-feedback-form").toggle("slide");
  });
  $("#user-feedback-form form").on('submit', function(event) {
    event.preventDefault();
    $("#user-feedback-form").css('visibility', 'hidden');
    $("#user-feedback-spinner").toggle(true);
    var $form = $(this);
    var data = { issue: $('#issue').val(), kind: $('#feedback_kind').val() };
    data.url = window.location.href;
    data.timeOpened = new Date();
    data.timezone = (new Date()).getTimezoneOffset()/60;
    data.pageon = window.location.pathname;
    data.referrer = document.referrer;
    data.previousSites = history.length;
    data.browserName = navigator.appName;
    data.browserEngine = navigator.product;
    data.browserVersion1a = navigator.appVersion;
    data.browserVersion1b = navigator.userAgent;
    data.browserLanguage = navigator.language;
    data.browserOnline = navigator.onLine;
    data.browserPlatform = navigator.platform;
    data.javaEnabled = navigator.javaEnabled();
    data.dataCookiesEnabled = navigator.cookieEnabled;
    data.dataCookies1 = document.cookie;
    data.dataCookies2 = decodeURIComponent(document.cookie.split(";"));
    data.dataStorage = localStorage;
    data.sizeScreenW = screen.width;
    data.sizeScreenH = screen.height;
    data.sizeDocW = document.width;
    data.sizeDocH = document.height;
    data.sizeInW = window.innerWidth;
    data.sizeInH = window.innerHeight;
    data.sizeAvailW = screen.availWidth;
    data.sizeAvailH = screen.availHeight;
    data.scrColorDepth = screen.colorDepth;
    data.scrPixelDepth = screen.pixelDepth;
    html2canvas(document.body, {
      onrendered: function(canvas) {
        data.screenshot = canvas.toDataURL();
        $.ajax({
          type: $form.attr('method'),
          url: $form.attr('action'),
          processData: false,
          data: window.JSON.stringify(data),
          dataType: "json",
          contentType: "application/json",
        }).always(function(data) {
          $("#user-feedback-spinner").toggle(false);
          $("#user-feedback-form").css('visibility', 'visible').toggle("slide").find("textarea").val('');
        }).done(function(data) {
          alert('Thanks for submitting your feedback ! Our reference is: {T' + data.task_id + "}");
        }).fail(function() {
          alert('Sorry there was an error submitting your feedback ! Please try again later.');
        });
      }
    });
  });
});
