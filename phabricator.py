#!/usr/bin/env python
# coding=utf-8

# ***** BEGIN LICENSE BLOCK *****
# This file is part of wuf2p (webapp user feedback to phabricator)
# (C) Copyright 2016 Paolo Greppi simevo.com
# ***** END LICENSE BLOCK ***** */

""" phabricator integration """

import requests

phabricator_instance = 'phabricator.example.com'
api_token = 'api-aaaaaaaaaaaaaaaaaaaaaaaaaaaa'
projectPHID = "PHID-PROJ-aaaaaaaaaaaaaaaaaaaa"


def file_upload(s, name, encoded):
    data = {'api.token': api_token,
            'name': name,
            'data_base64': encoded}
    url1 = 'https://' + phabricator_instance + '/api/file.upload'
    r1 = s.post(url1, data)
    r1.raise_for_status()
    results1 = r1.json()
    error_info1 = results1['error_info']
    if error_info1:
        print error_info1
        return -1

    phid = results1['result']
    payload = {'api.token': api_token,
               'phid': phid}
    url2 = 'https://' + phabricator_instance + '/api/file.info'
    r2 = s.get(url2, params=payload)
    r2.raise_for_status()
    results2 = r2.json()
    error_info2 = results2['error_info']
    if error_info2:
        print error_info2
        return -2

    file_id = results2['result']['id']
    return file_id


def create_task(s, title, description):
    data = {'api.token': api_token,
            'title': title,
            'description': description,
            'projectPHIDs[]': [projectPHID]}
    url = 'https://' + phabricator_instance + '/api/maniphest.createtask'
    req = requests.Request('POST', url, data=data)
    prepped = s.prepare_request(req)
    resp = s.send(prepped)
    resp.raise_for_status()
    results = resp.json()
    error_info = results['error_info']
    if error_info:
        print error_info
        return {}
    uri = results['result']['uri']
    task_id = results['result']['id']
    return {"uri": uri, "task_id": task_id}
