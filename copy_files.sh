#!/bin/sh
set -e
cp bower_components/bootstrap/dist/css/bootstrap.min.css css/.
cp bower_components/bootstrap/dist/css/bootstrap.min.css.map css/.
cp bower_components/bootstrap/dist/css/bootstrap-theme.min.css css/.
cp bower_components/bootstrap/dist/css/bootstrap-theme.min.css.map css/.
cp bower_components/bootstrap/dist/js/bootstrap.min.js js/.
cp bower_components/jquery/dist/jquery.min.js js/.
cp bower_components/jquery/dist/jquery.min.map js/.
mkdir -p fonts
cp bower_components/bootstrap/dist/fonts/glyphicons-halflings-regular.* fonts/.
cp bower_components/html2canvas/build/html2canvas.min.js js/.
