#!/usr/bin/env python
# coding=utf-8

# ***** BEGIN LICENSE BLOCK *****
# This file is part of wuf2p (webapp user feedback to phabricator)
# (C) Copyright 2016 Paolo Greppi simevo.com
# ***** END LICENSE BLOCK ***** */

""" wuf2p backend """

from __future__ import division

import bottle
import phabricator
import requests
import socket
import json


def render_json(request, response, d, code):
    """ sends the supplied JSON object as response
    if the request header accepts application/json, the JSON is dumped in compact form,
    else it is prettyprinted and wrapped in a <pre> HTML element
    """
    accept = request.headers['accept']
    if accept.find('application/json') >= 0:
        return bottle.HTTPResponse(status=code, body=json.dumps(d), headers={"content-type": "application/json"})
    else:
        return bottle.HTTPResponse(status=code, body='<pre>' + json.dumps(d, indent=2) + '</pre>\n')


@bottle.route('/feedback', method='POST')
def send_feedback():
    """ upload screenshot and create a task on the phabricator instance

    input: the data from feedback.js encoded as JSON
    output: the task data

    sample call:
    curl -k -i -X POST data='{"issue": "qqqqqqqqq", "screenshot": "oooooooo"}' http://localhost/api/feedback
    """
    data = bottle.request.json
    with open('/etc/machine-id', 'r') as machine_id_file:
        data['machine_id'] = machine_id_file.read()[:-1]  # strip newline
    data['fqdn'] = socket.getfqdn()

    s = requests.Session()

    encoded_image = None
    if 'screenshot' in data:
        # skip the initial 'data:image/png;base64,' prolog
        encoded_image = data['screenshot'][22:]
        # delete the screenshot from the dictionary so that it does not get passed to the bottle SimpleTemplate Engine
        del data['screenshot']
        screenshot_id = phabricator.file_upload(s, "screenshot", encoded_image)
        if screenshot_id > 0:
            data['screenshot_id'] = screenshot_id
        else:
            raise bottle.HTTPError(status=540, body="error sending feedback")

    description = bottle.template('views/wuf2p.tpl', data)
    kind = data.get('kind', 'generic')
    title = ('user feedback: ' + kind) if kind else 'user feedback'
    task = phabricator.create_task(s, title, description)
    if 'task_id' in task:
        data['task_id'] = task['task_id']
        return render_json(bottle.request, bottle.response, task, 201)
    else:
        raise bottle.HTTPError(status=541, body="error sending feedback")


@bottle.get('/<filename:re:.*\.(css|js|jpg|png|gif|ico|eot|ttf|woff|svg)>')
def images(filename):
    return bottle.static_file(filename, root='.')


@bottle.get('/')
def index():
    return bottle.static_file('index.html', root='.')


if __name__ == '__main__':
    bottle.BaseRequest.MEMFILE_MAX = 1024 * 1024
    bottle.debug(True)
    bottle.run(host='127.0.0.1', port=8080, reloader=True, server='cherrypy')
